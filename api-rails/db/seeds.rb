roles = Role.create([
  { name: 'Administrator' },
  { name: 'Employee' }])

admin = User.create({
  name: 'Augusto',
  username: 'admin',
  password: 'password',
  role: roles.first
})

5.times do
  User.create({
    name: Faker::Name.name,
    username: Faker::Lorem.word,
    password: 'password',
    role: roles.last
  })
end

25.times do 
  PerformanceEvaluation.create({
    description: Faker::Lorem.paragraph,
    author: admin,
    employee: User.where(:role_id => roles.last).shuffle.first
  })
end

125.times do |index|
  Feedback.create({
    description: index % 2 == 0 ? nil : Faker::Lorem.paragraph,
    performance_evaluation: PerformanceEvaluation.all.shuffle.first,
    user: User.where(:role_id => roles.last).shuffle.first
  })
end
