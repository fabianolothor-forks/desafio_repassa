class CreatePerformanceEvaluations < ActiveRecord::Migration[6.0]
  def change
    create_table :performance_evaluations do |t|
      t.string :description
      t.references :author, null: false
      t.references :employee, null: false

      t.timestamps
    end
  end
end
