module Api
  module V1
    class FeedbacksController < ApplicationController
      def index
        feedbacks = Feedback.order('created_at DESC')
        
        render json: feedbacks.to_json(:include => [:user, :performance_evaluation => { :include => [:author, :employee] }])
      end

      def show
        feedback = Feedback.find(params[:id])

        render json: feedback.to_json(:include => [:user, :performance_evaluation => { :include => [:author, :employee] }])
      end

      def create
        feedback = Feedback.new(feedback_params)

        if feedback.save
          render json: feedback.to_json(:include => [:user, :performance_evaluation => { :include => [:author, :employee] }])
        else
          render json: feedback.errors, status: :unprocessable_entity
        end
      end

      def update
        feedback = Feedback.find(params[:id])

        if feedback.update_attributes(feedback_params)
          render json: feedback.to_json(:include => [:user, :performance_evaluation => { :include => [:author, :employee] }])
        else
          render json: feedback.errors, status: :unprocessable_entity
        end
      end

      def destroy
        feedback = Feedback.find(params[:id])
        feedback.destroy

        render json: feedback.to_json(:include => [:user, :performance_evaluation => { :include => [:author, :employee] }])
      end

      def by_user
        feedbacks = Feedback.where(:user_id => params[:id], :description => nil)

        if feedbacks
          render json: feedbacks.to_json(:include => [:user, :performance_evaluation => { :include => [:author, :employee] }])
        else
          render json: feedbacks, status: :not_found
        end
      end

      private

      def feedback_params
        params.permit(:description, :performance_evaluation_id, :user_id)
      end

    end
  end
end
