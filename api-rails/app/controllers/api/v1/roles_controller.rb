module Api
  module V1
    class RolesController < ApplicationController
      def index
        roles = Role.order('created_at ASC')
        
        render json: roles
      end

      def show
        role = Role.find(params[:id])

        render json: role
      end

      def create
        role = Role.new(role_params)

        if role.save
          render json: role
        else
          render json: role.errors, status: :unprocessable_entity
        end
      end

      def update
        role = Role.find(params[:id])

        if role.update_attributes(role_params)
          render json: role
        else
          render json: role.errors, status: :unprocessable_entity
        end
      end

      def destroy
        role = Role.find(params[:id])
        role.destroy

        render json: role
      end

      private

      def role_params
        params.permit(:name)
      end

    end
  end
end