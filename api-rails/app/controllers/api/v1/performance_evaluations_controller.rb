module Api
  module V1
    class PerformanceEvaluationsController < ApplicationController
      def index
        performance_evaluations = PerformanceEvaluation.order('created_at DESC')
        
        render json: performance_evaluations.to_json(:include => [:author, :employee, :feedbacks => { :include => [:user] }])
      end

      def show
        performance_evaluation = PerformanceEvaluation.find(params[:id])

        render json: performance_evaluation.to_json(:include => [:author, :employee, :feedbacks => { :include => [:user] }])
      end

      def create
        performance_evaluation = PerformanceEvaluation.new(performance_evaluation_params)

        if performance_evaluation.save
          render json: performance_evaluation.to_json(:include => [:author, :employee, :feedbacks => { :include => [:user] }])
        else
          render json: performance_evaluation.errors, status: :unprocessable_entity
        end
      end

      def update
        performance_evaluation = PerformanceEvaluation.find(params[:id])

        if performance_evaluation.update_attributes(performance_evaluation_params)
          render json: performance_evaluation.to_json(:include => [:author, :employee, :feedbacks => { :include => [:user] }])
        else
          render json: performance_evaluation.errors, status: :unprocessable_entity
        end
      end

      def destroy
        performance_evaluation = PerformanceEvaluation.find(params[:id])
        performance_evaluation.destroy

        render json: performance_evaluation.to_json(:include => [:author, :employee, :feedbacks => { :include => [:user] }])
      end

      def by_user
        performance_evaluations = PerformanceEvaluation.where(:employee_id => params[:id])
        
        if performance_evaluations
          render json: performance_evaluations.to_json(:include => [:author, :employee, :feedbacks => { :include => [:user] }])
        else
          render json: performance_evaluations, status: :not_found
        end
      end

      private

      def performance_evaluation_params
        params.permit(:description, :author_id, :employee_id)
      end

    end
  end
end
