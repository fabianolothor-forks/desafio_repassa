module Api
  module V1
    class UsersController < ApplicationController
      def index
        users = User.order('created_at DESC')
        
        render json: users.to_json(:include => :role)
      end

      def show
        user = User.find(params[:id])

        render json: user.to_json(:include => :role)
      end

      def create
        user = User.new(user_params)

        if user.save
          render json: user.to_json(:include => :role)
        else
          render json: user.errors, status: :unprocessable_entity
        end
      end

      def update
        user = User.find(params[:id])

        if user.update_attributes(user_params)
          render json: user.to_json(:include => :role)
        else
          render json: user.errors, status: :unprocessable_entity
        end
      end

      def destroy
        user = User.find(params[:id])
        user.destroy

        render json: user.to_json(:include => :role)
      end

      def sign_in
        user = User.find_by(username: params[:username], password: params[:password])
        
        if user
          render json: user.to_json(:include => :role)
        else
          render json: user, status: :not_found
        end
      end

      private

      def user_params
        params.permit(:name, :username, :password, :role_id)
      end

    end
  end
end
