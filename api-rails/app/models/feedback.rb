class Feedback < ApplicationRecord
  belongs_to :performance_evaluation, optional: true
  belongs_to :user, optional: true

  validates :performance_evaluation_id, presence: true
  validates :user_id, presence: true
end
