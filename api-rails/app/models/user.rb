class User < ApplicationRecord
  belongs_to :role, optional: true

  has_many :author_performance_evaluations, :class_name => 'PerformanceEvaluation', :foreign_key => 'author_id'
  has_many :employee_performance_evaluations, :class_name => 'PerformanceEvaluation', :foreign_key => 'employee_id'

  validates :name, presence: true
  validates :username, presence: true, uniqueness: true
  validates :password, presence: true
  validates :role_id, presence: true
end
