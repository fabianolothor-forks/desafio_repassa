class PerformanceEvaluation < ApplicationRecord
  belongs_to :author, class_name: 'User', optional: true
  belongs_to :employee, class_name: 'User', optional: true

  has_many :feedbacks, dependent: :delete_all

  validates :description, presence: true
  validates :author_id, presence: true
  validates :employee_id, presence: true
end
