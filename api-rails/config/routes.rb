Rails.application.routes.draw do
  namespace 'api' do
    namespace 'v1' do
      resources :feedbacks
      resources :performance_evaluations
      resources :roles
      resources :users
      
      get '/feedbacks/user/:id', to: 'feedbacks#by_user'
      get '/performance_evaluations/user/:id', to: 'performance_evaluations#by_user'
      post '/users/sign_in', to: 'users#sign_in'
    end
  end
end
