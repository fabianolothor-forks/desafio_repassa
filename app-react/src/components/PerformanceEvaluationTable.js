import React, { useContext } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import { APIHelper } from '../helpers';
import AddCircle from '@material-ui/icons/AddCircle';
import { AppContext } from '../contexts';
import { CircularProgress } from '@material-ui/core';
import Delete from '@material-ui/icons/Delete';
import Edit from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Visibility from '@material-ui/icons/Visibility';
import axios from 'axios';
import { useSnackbar } from 'notistack';
import { withRouter } from 'react-router-dom';

const StyledTableCell = withStyles(theme => ({
  body: {
    fontSize: 14
  }
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default
    }
  }
}))(TableRow);

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto'
  },
  table: {
    minWidth: 700
  }
}));

function PerformanceEvaluationTable({ history, reRender, performanceEvaluations = [] }) {
  const classes = useStyles();

  const { enqueueSnackbar } = useSnackbar();
  const { user: contextUser } = useContext(AppContext);

  return (
    <Paper className={classes.root}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell align="center">ID</StyledTableCell>
            <StyledTableCell align="center">Author</StyledTableCell>
            <StyledTableCell align="center">Employee</StyledTableCell>
            <StyledTableCell align="center">Feedbacks</StyledTableCell>
            <StyledTableCell align="center">Last Update</StyledTableCell>
            <StyledTableCell align="center">
              {contextUser.role.name === 'Administrator' ? (
                <IconButton
                  color="primary"
                  aria-label="add"
                  className={classes.margin}
                  onClick={function(event) {
                    event.preventDefault();

                    history.push(`/dashboard/performance-evaluation`);
                  }}
                >
                  <AddCircle fontSize="large" />
                </IconButton>
              ) : (
                'Actions'
              )}
            </StyledTableCell>
          </TableRow>
        </TableHead>
        {performanceEvaluations.length ? (
          <TableBody>
            {performanceEvaluations.map(function(performanceEvaluation) {
              const lastUpdate = new Date(performanceEvaluation.updated_at);

              return (
                <StyledTableRow key={performanceEvaluation.id}>
                  <StyledTableCell align="center">{performanceEvaluation.id}</StyledTableCell>
                  <StyledTableCell align="center">
                    {performanceEvaluation.author.name}
                  </StyledTableCell>
                  <StyledTableCell align="center">
                    {performanceEvaluation.employee.name}
                  </StyledTableCell>
                  <StyledTableCell align="center">
                    {
                      performanceEvaluation.feedbacks.filter(feedback => feedback.description)
                        .length
                    }
                    /{performanceEvaluation.feedbacks.length}
                    {contextUser.role.name === 'Administrator' ? (
                      <IconButton
                        aria-label="feedback"
                        className={classes.margin}
                        onClick={function(event) {
                          event.preventDefault();

                          history.push(
                            `/dashboard/performance-evaluation/${performanceEvaluation.id}/feedback`
                          );
                        }}
                      >
                        <AddCircle />
                      </IconButton>
                    ) : null}
                  </StyledTableCell>
                  <StyledTableCell align="center">{`${lastUpdate.toLocaleDateString()} ${lastUpdate.toLocaleTimeString()}`}</StyledTableCell>
                  <StyledTableCell align="center">
                    <IconButton
                      aria-label="view"
                      className={classes.margin}
                      onClick={function(event) {
                        event.preventDefault();

                        history.push(
                          `/dashboard/performance-evaluation/${performanceEvaluation.id}/view`
                        );
                      }}
                    >
                      <Visibility fontSize="large" />
                    </IconButton>
                    {contextUser.role.name === 'Administrator' ? (
                      <>
                        <IconButton
                          aria-label="edit"
                          className={classes.margin}
                          onClick={function(event) {
                            event.preventDefault();

                            history.push(
                              `/dashboard/performance-evaluation/${performanceEvaluation.id}`
                            );
                          }}
                        >
                          <Edit fontSize="large" />
                        </IconButton>
                        <IconButton
                          aria-label="delete"
                          className={classes.margin}
                          onClick={function(event) {
                            event.preventDefault();

                            if (
                              window.confirm(
                                `You are deleting the Performance Evaluation "${performanceEvaluation.id}" from the "${performanceEvaluation.employee.name}.".\n\nAre you sure?`
                              )
                            ) {
                              axios
                                .delete(
                                  APIHelper.performanceEvaluation(performanceEvaluation.id)
                                )
                                .then(() => {
                                  reRender(performanceEvaluation.id);
                                })
                                .then(function(response) {
                                  enqueueSnackbar(
                                    `"Performance Evaluation  ${performanceEvaluation.id}" was deleted successfully!`,
                                    {
                                      variant: 'success'
                                    }
                                  );
                                });
                            }
                          }}
                        >
                          <Delete fontSize="large" />
                        </IconButton>
                      </>
                    ) : null}
                  </StyledTableCell>
                </StyledTableRow>
              );
            })}
          </TableBody>
        ) : (
          <TableFooter>
            <StyledTableRow>
              <StyledTableCell colSpan={6} style={{ textAlign: 'center' }}>
                <CircularProgress />
                <br />
                Notthing to Show Here
              </StyledTableCell>
            </StyledTableRow>
          </TableFooter>
        )}
      </Table>
    </Paper>
  );
}

export default withRouter(PerformanceEvaluationTable);
