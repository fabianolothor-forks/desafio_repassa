import { Footer, Header } from '.';

import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

function Template({ children }) {
  return (
    <>
      <CssBaseline />
      <Header />
      <Container>{children}</Container>
      <Footer />
    </>
  );
}

export default Template;
