import React, { useContext } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import { AppContext } from '../contexts';
import { CircularProgress } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Send from '@material-ui/icons/Send';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { withRouter } from 'react-router-dom';

const StyledTableCell = withStyles(theme => ({
  body: {
    fontSize: 14
  }
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default
    }
  }
}))(TableRow);

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto'
  },
  table: {
    minWidth: 700
  }
}));

function RequestedFeedbackTable({ history, reRender, requestedFeedbacks = [] }) {
  const classes = useStyles();

  const { user: contextUser } = useContext(AppContext);
  console.log(requestedFeedbacks);
  return (
    <Paper className={classes.root}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell align="center">ID</StyledTableCell>
            <StyledTableCell align="center">Feedback To</StyledTableCell>
            <StyledTableCell align="center">Requested By</StyledTableCell>
            <StyledTableCell align="center">Requested At</StyledTableCell>
            <StyledTableCell align="center">Last Update</StyledTableCell>
            <StyledTableCell align="center">Actions</StyledTableCell>
          </TableRow>
        </TableHead>
        {requestedFeedbacks.length ? (
          <TableBody>
            {requestedFeedbacks.map(function(feedback) {
              const createdAt = new Date(feedback.created_at);
              const lastUpdate = new Date(feedback.updated_at);

              return (
                <StyledTableRow key={feedback.id}>
                  <StyledTableCell align="center">{feedback.id}</StyledTableCell>
                  <StyledTableCell align="center">
                    {feedback.performance_evaluation.employee.name}
                  </StyledTableCell>
                  <StyledTableCell align="center">
                    {feedback.performance_evaluation.author.name}
                  </StyledTableCell>
                  <StyledTableCell align="center">{`${createdAt.toLocaleDateString()} ${createdAt.toLocaleTimeString()}`}</StyledTableCell>
                  <StyledTableCell align="center">{`${lastUpdate.toLocaleDateString()} ${lastUpdate.toLocaleTimeString()}`}</StyledTableCell>
                  <StyledTableCell align="center">
                    <IconButton
                      aria-label="view"
                      className={classes.margin}
                      onClick={function(event) {
                        event.preventDefault();

                        history.push(`/dashboard/feedback/${feedback.id}`);
                      }}
                    >
                      <Send fontSize="large" />
                    </IconButton>
                  </StyledTableCell>
                </StyledTableRow>
              );
            })}
          </TableBody>
        ) : (
          <TableFooter>
            <StyledTableRow>
              <StyledTableCell colSpan={6} style={{ textAlign: 'center' }}>
                <CircularProgress />
                <br />
                Notthing to Show Here
              </StyledTableCell>
            </StyledTableRow>
          </TableFooter>
        )}
      </Table>
    </Paper>
  );
}

export default withRouter(RequestedFeedbackTable);
