import { PerformanceEvaluationTable, RequestedFeedbackTable } from '.';
import React, { useContext, useEffect, useState } from 'react';

import { APIHelper } from '../helpers';
import AppBar from '@material-ui/core/AppBar';
import { AppContext } from '../contexts';
import Box from '@material-ui/core/Box';
import Forum from '@material-ui/icons/Forum';
import ListAlt from '@material-ui/icons/ListAlt';
import PropTypes from 'prop-types';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-prevent-tabpanel-${index}`}
      aria-labelledby={`scrollable-prevent-tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function a11yProps(index) {
  return {
    id: `scrollable-prevent-tab-${index}`,
    'aria-controls': `scrollable-prevent-tabpanel-${index}`
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper
  }
}));

function UserDashboard({ match }) {
  const [tab, setTab] = useState(0);
  const [requestedFeedbacks, setRequestedFeedbacks] = useState([]);
  const [performanceEvaluations, setPerformanceEvaluations] = useState([]);

  const classes = useStyles();

  const { tab: selectedTab } = match.params || {};
  const { user } = useContext(AppContext);

  const handleChange = (event, newValue) => {
    setTab(newValue);
  };

  useEffect(
    function() {
      axios.get(APIHelper.performanceEvaluations(user.id)).then(function(response) {
        setPerformanceEvaluations(response.data);
      });

      axios.get(APIHelper.feedbacks(user.id)).then(function(response) {
        setRequestedFeedbacks(response.data);
      });

      setTab((selectedTab || 'pe') === 'pe' ? 0 : 1);
    },
    [selectedTab, user.id]
  );

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs
          value={tab}
          onChange={handleChange}
          // variant="scrollable"
          scrollButtons="off"
          aria-label="scrollable prevent tabs example"
          centered
        >
          <Tab
            icon={<ListAlt />}
            label="Performance Evaluations"
            aria-label="performanceEvaluation"
            {...a11yProps(1)}
          />
          <Tab
            icon={<Forum />}
            label="Requested Feedbacks"
            aria-label="requested-feedbacks"
            {...a11yProps(0)}
          />
        </Tabs>
      </AppBar>
      <TabPanel value={tab} index={0} style={{ textAlign: 'center' }}>
        <PerformanceEvaluationTable performanceEvaluations={performanceEvaluations} />
      </TabPanel>
      <TabPanel value={tab} index={1} style={{ textAlign: 'center' }}>
        <RequestedFeedbackTable requestedFeedbacks={requestedFeedbacks} />
      </TabPanel>
    </div>
  );
}

export default withRouter(UserDashboard);
