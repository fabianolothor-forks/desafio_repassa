import React, { useContext } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import { APIHelper } from '../helpers';
import AddCircle from '@material-ui/icons/AddCircle';
import { AppContext } from '../contexts';
import { CircularProgress } from '@material-ui/core';
import Delete from '@material-ui/icons/Delete';
import Edit from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import axios from 'axios';
import { useSnackbar } from 'notistack';
import { withRouter } from 'react-router-dom';

const StyledTableCell = withStyles(theme => ({
  body: {
    fontSize: 14
  }
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default
    }
  }
}))(TableRow);

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto'
  },
  table: {
    minWidth: 700
  }
}));

function UsersTable({ history, reRender, users = [] }) {
  const classes = useStyles();

  const { enqueueSnackbar } = useSnackbar();
  const { user: contextUser } = useContext(AppContext);

  return users.length ? (
    <Paper className={classes.root}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell align="center">ID</StyledTableCell>
            <StyledTableCell align="center">Name</StyledTableCell>
            <StyledTableCell align="center">Username</StyledTableCell>
            <StyledTableCell align="center">Role</StyledTableCell>
            <StyledTableCell align="center">Last Update</StyledTableCell>
            <StyledTableCell align="center">
              <IconButton
                color="primary"
                aria-label="add"
                className={classes.margin}
                onClick={function(event) {
                  event.preventDefault();

                  history.push(`/dashboard/user`);
                }}
              >
                <AddCircle fontSize="large" />
              </IconButton>
            </StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {users.map(function(user) {
            const lastUpdate = new Date(user.updated_at);

            return (
              <StyledTableRow key={user.id}>
                <StyledTableCell align="center">{user.id}</StyledTableCell>
                <StyledTableCell align="center">{user.name}</StyledTableCell>
                <StyledTableCell align="center">{user.username}</StyledTableCell>
                <StyledTableCell align="center">{user.role.name}</StyledTableCell>
                <StyledTableCell align="center">{`${lastUpdate.toLocaleDateString()} ${lastUpdate.toLocaleTimeString()}`}</StyledTableCell>
                <StyledTableCell align="center">
                  <IconButton
                    aria-label="edit"
                    className={classes.margin}
                    onClick={function(event) {
                      event.preventDefault();

                      history.push(`/dashboard/user/${user.id}`);
                    }}
                  >
                    <Edit fontSize="large" />
                  </IconButton>
                  {contextUser.id !== user.id && (
                    <IconButton
                      aria-label="delete"
                      className={classes.margin}
                      onClick={function(event) {
                        event.preventDefault();

                        if (
                          window.confirm(
                            `You are deleting the ${user.role.name} "${user.name}".\n\nAre you sure?`
                          )
                        ) {
                          axios
                            .delete(APIHelper.user(user.id))
                            .then(() => {
                              reRender(user.id);
                            })
                            .then(function(response) {
                              enqueueSnackbar(`"${user.name}" was deleted successfully!`, {
                                variant: 'success'
                              });
                            });
                        }
                      }}
                    >
                      <Delete fontSize="large" />
                    </IconButton>
                  )}
                </StyledTableCell>
              </StyledTableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  ) : (
    <CircularProgress />
  );
}

export default withRouter(UsersTable);
