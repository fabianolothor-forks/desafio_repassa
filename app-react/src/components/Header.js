import React, { useContext } from 'react';

import AppBar from '@material-ui/core/AppBar';
import { AppContext } from '../contexts';
import Button from '@material-ui/core/Button';
import Home from '@material-ui/icons/Home';
import IconButton from '@material-ui/core/IconButton';
import Link from '@material-ui/core/Link';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white
    },
    ul: {
      margin: 0,
      padding: 0
    },
    li: {
      listStyle: 'none'
    }
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`
  },
  toolbar: {
    flexWrap: 'wrap'
  },
  toolbarTitle: {
    flexGrow: 1
  },
  link: {
    margin: theme.spacing(1, 1.5)
  },
  heroContent: {
    padding: theme.spacing(8, 0, 6)
  },
  cardHeader: {
    backgroundColor: theme.palette.grey[200]
  },
  cardPricing: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'baseline',
    marginBottom: theme.spacing(2)
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up('sm')]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6)
    }
  }
}));

function Header({ history }) {
  const classes = useStyles();

  const { app, user, updateUser } = useContext(AppContext);

  return (
    <AppBar position="static" color="default" elevation={0} className={classes.appBar}>
      <Toolbar className={classes.toolbar}>
        <Typography variant="h6" color="inherit" noWrap className={classes.toolbarTitle}>
          <Link
            href="/"
            variant="h6"
            onClick={function(event) {
              event.preventDefault();

              history.push('/');
            }}
          >
            <IconButton color="primary" className={classes.margin}>
              <Home fontSize="small" />
            </IconButton>
            {app.name}
          </Link>
        </Typography>

        {user.isLogged ? (
          <Typography variant="body2" color="inherit" noWrap>
            Logged as {user.name}
          </Typography>
        ) : null}

        <Button
          href="#"
          color="primary"
          variant="outlined"
          className={classes.link}
          onClick={function(event) {
            event.preventDefault();

            if (user.isLogged) {
              updateUser({ isLogged: false });
            }

            history.push('/sign-in');
          }}
        >
          {user.isLogged ? 'Logoff' : 'Login'}
        </Button>
      </Toolbar>
    </AppBar>
  );
}

export default withRouter(Header);
