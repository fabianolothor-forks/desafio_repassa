import { PerformanceEvaluationTable, UsersTable } from '.';
import React, { useEffect, useState } from 'react';

import { APIHelper } from '../helpers';
import AppBar from '@material-ui/core/AppBar';
import AssignmentInd from '@material-ui/icons/AssignmentInd';
import Box from '@material-ui/core/Box';
import ListAlt from '@material-ui/icons/ListAlt';
import PropTypes from 'prop-types';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-prevent-tabpanel-${index}`}
      aria-labelledby={`scrollable-prevent-tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function a11yProps(index) {
  return {
    id: `scrollable-prevent-tab-${index}`,
    'aria-controls': `scrollable-prevent-tabpanel-${index}`
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper
  }
}));

function AdminDashboard({ match }) {
  const [tab, setTab] = useState(0);
  const [users, setUsers] = useState([]);
  const [performanceEvaluations, setPerformanceEvaluations] = useState([]);
  const [reRenderDashboard, forceReRenderDashboard] = useState(false);

  const classes = useStyles();

  const { tab: selectedTab } = match.params || {};

  const handleChange = (event, newValue) => {
    setTab(newValue);
  };

  useEffect(
    function() {
      axios.get(APIHelper.users()).then(function(response) {
        setUsers(response.data);
      });

      axios.get(APIHelper.performanceEvaluations()).then(function(response) {
        setPerformanceEvaluations(response.data);
      });

      setTab((selectedTab || 'u') === 'u' ? 0 : 1);
    },
    [reRenderDashboard, selectedTab]
  );

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs
          value={tab}
          onChange={handleChange}
          // variant="scrollable"
          scrollButtons="off"
          aria-label="scrollable prevent tabs example"
          centered
        >
          <Tab icon={<AssignmentInd />} label="Users" aria-label="user" {...a11yProps(0)} />
          <Tab
            icon={<ListAlt />}
            label="Performance Evaluations"
            aria-label="performanceEvaluation"
            {...a11yProps(1)}
          />
        </Tabs>
      </AppBar>
      <TabPanel value={tab} index={0} style={{ textAlign: 'center' }}>
        <UsersTable users={users} reRender={forceReRenderDashboard} />
      </TabPanel>
      <TabPanel value={tab} index={1} style={{ textAlign: 'center' }}>
        <PerformanceEvaluationTable
          performanceEvaluations={performanceEvaluations}
          reRender={forceReRenderDashboard}
        />
      </TabPanel>
    </div>
  );
}

export default withRouter(AdminDashboard);
