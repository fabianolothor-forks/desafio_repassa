import React, { useState } from 'react';

const AppContext = React.createContext();

const initialState = {
  app: {
    name: 'Desafio Repassa - Fabiano'
  },
  user: {
    isLogged: false
  }
};

export function AppCustomContextProvider({ children }) {
  const [state, setState] = useState(initialState);

  function updateUser(payload) {
    setState({ ...state, user: { ...state.user, ...payload } });
  }

  return (
    <AppContext.Provider
      value={{
        ...state,
        updateUser: updateUser
      }}
    >
      {children}
    </AppContext.Provider>
  );
}

export default AppContext;
