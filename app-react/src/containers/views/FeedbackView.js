import React, { useContext, useEffect, useState } from 'react';

import { APIHelper } from '../../helpers';
import AddCircle from '@material-ui/icons/AddCircle';
import { AppContext } from '../../contexts';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Edit from '@material-ui/icons/Edit';
import Grid from '@material-ui/core/Grid';
import InputBase from '@material-ui/core/InputBase';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { Redirect } from 'react-router-dom';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

function FeedbackView({ history, match }) {
  const [feedback, setFeedback] = useState({});

  const classes = useStyles();

  const { enqueueSnackbar } = useSnackbar();
  const { user: contextUser } = useContext(AppContext);
  const { idFeedback } = match.params || {};

  function handleChange(e) {
    setFeedback({
      ...feedback,
      [e.target.name]: e.target.value
    });
  }

  useEffect(
    function() {
      if (idFeedback) {
        axios.get(APIHelper.feedback(idFeedback)).then(function(response) {
          if (response.data.user_id === contextUser.id) {
            setFeedback(response.data);
          }
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [idFeedback]
  );

  return contextUser.isLogged && contextUser.role.name !== 'Administrator' ? (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <AddCircle />
        </Avatar>
        <Typography component="h1" variant="h5" align="center">
          Send Feedback
        </Typography>
        <form
          className={classes.form}
          onSubmit={async function(event) {
            event.preventDefault();

            const target = event.target;
            const data = new FormData(target);

            const handleThen = function(response) {
              enqueueSnackbar(
                `Feedback sent to "${response.data.performance_evaluation.employee.name}"!`,
                {
                  variant: 'success'
                }
              );

              history.push('/dashboard/f');
            };
            const handleCatch = function(response) {
              enqueueSnackbar('Fail! Please, check all the fields and try again.', {
                variant: 'error'
              });
            };

            await axios
              .put(APIHelper.feedback(idFeedback), data)
              .then(handleThen)
              .catch(handleCatch);
          }}
        >
          <Grid container spacing={2}>
            {feedback.performance_evaluation && (
              <>
                <Grid item xs={12}>
                  <TextField
                    defaultValue={idFeedback && 'Loading...'}
                    value={feedback.performance_evaluation.description}
                    onChange={handleChange}
                    autoComplete="performance_evaluation"
                    name="performance_evaluation"
                    variant="outlined"
                    required
                    fullWidth
                    disabled
                    id="performance_evaluation"
                    label="Performance Evaluation"
                    autoFocus
                    multiline
                  />
                </Grid>
                <Grid item xs={12}>
                  <InputLabel id="user_id" required>
                    Employee
                  </InputLabel>
                  <Select
                    required
                    fullWidth
                    disabled
                    defaultValue={idFeedback && 'Loading...'}
                    value={`${feedback.performance_evaluation.employee_id}`}
                    onChange={handleChange}
                    labelId="user_id"
                    id="user_id"
                    name="user_id"
                    input={<InputBase />}
                  >
                    <MenuItem value={feedback.performance_evaluation.employee.id}>
                      {feedback.performance_evaluation.employee.name}
                    </MenuItem>
                    })}
                  </Select>
                </Grid>
              </>
            )}
            <Grid item xs={12}>
              <TextField
                value={feedback.description}
                onChange={handleChange}
                autoComplete="description"
                name="description"
                variant="outlined"
                required
                fullWidth
                id="description"
                label="Feedback"
                autoFocus
                multiline
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Send Feedback
          </Button>
        </form>
      </div>
    </Container>
  ) : (
    <Redirect to="/" push />
  );
}

export default FeedbackView;
