import Avatar from '@material-ui/core/Avatar';
import Container from '@material-ui/core/Container';
import React from 'react';
import SentimentVeryDissatisfied from '@material-ui/icons/SentimentVeryDissatisfied';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(5),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

function HomeView() {
  const classes = useStyles();

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <SentimentVeryDissatisfied fontSize="large" />
        </Avatar>
        <Typography component="h1" variant="h5" align="center">
          Error 404
          <br />
          Page Not Found
        </Typography>
      </div>
    </Container>
  );
}

export default HomeView;
