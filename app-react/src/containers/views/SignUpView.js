import React, { useContext, useEffect, useState } from 'react';

import { APIHelper } from '../../helpers';
import { AppContext } from '../../contexts';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import InputBase from '@material-ui/core/InputBase';
import InputLabel from '@material-ui/core/InputLabel';
import Link from '@material-ui/core/Link';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import MenuItem from '@material-ui/core/MenuItem';
import { Redirect } from 'react-router-dom';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

function SignUp({ history }) {
  const [roles, setRoles] = useState([]);

  const classes = useStyles();

  const { enqueueSnackbar } = useSnackbar();
  const { user, updateUser } = useContext(AppContext);

  useEffect(function() {
    axios.get(APIHelper.roles()).then(function(response) {
      setRoles(response.data.filter(role => role.name !== 'Administrator'));
    });
  }, []);

  return user.isLogged ? (
    <Redirect to="/dashboard" push />
  ) : (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form
          className={classes.form}
          onSubmit={async function(event) {
            event.preventDefault();

            const target = event.target;
            const data = new FormData(target);

            await axios
              .post(APIHelper.users(), data)
              .then(function(response) {
                updateUser({ ...response.data, isLogged: true });

                enqueueSnackbar(`Welcome!`, { autoHideDuration: 1000, variant: 'success' });

                history.push('/dashboard');
              })
              .catch(function() {
                enqueueSnackbar('Fail! Please, check all the fields and try again.', {
                  variant: 'error'
                });
              });
          }}
        >
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                autoComplete="name"
                name="name"
                variant="outlined"
                required
                fullWidth
                id="name"
                label="Name"
                autoFocus
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="username"
                label="Username"
                name="username"
                autoComplete="username"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="password"
              />
            </Grid>
            <Grid item xs={12}>
              <InputLabel id="role_id" required>
                Role
              </InputLabel>
              <Select
                fullWidth
                required
                readOnly
                disabled
                labelId="role_id"
                id="role_id"
                name="role_id"
                input={<InputBase />}
                value={roles.length && roles[0].id}
              >
                {roles.map((role, index) => {
                  return (
                    <MenuItem key={`role_${index}`} value={role.id}>
                      {role.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link
                href="/sign-in"
                variant="body2"
                onClick={function(event) {
                  event.preventDefault();

                  history.push('/sign-in');
                }}
              >
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}

export default SignUp;
