import React, { useContext, useEffect, useState } from 'react';

import { APIHelper } from '../../helpers';
import AddCircle from '@material-ui/icons/AddCircle';
import { AppContext } from '../../contexts';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Edit from '@material-ui/icons/Edit';
import Grid from '@material-ui/core/Grid';
import InputBase from '@material-ui/core/InputBase';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { Redirect } from 'react-router-dom';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

function UserView({ history, match }) {
  const [roles, setRoles] = useState([]);
  const [user, setUser] = useState({});

  const classes = useStyles();

  const { enqueueSnackbar } = useSnackbar();
  const { user: contextUser } = useContext(AppContext);
  const { idUser } = match.params || {};

  function handleChange(e) {
    setUser({
      ...user,
      [e.target.name]: e.target.value
    });
  }

  useEffect(
    function() {
      axios.get(APIHelper.roles()).then(function(response) {
        setRoles(response.data);
      });

      if (idUser) {
        axios.get(APIHelper.user(idUser)).then(function(response) {
          setUser(response.data);
        });
      }
    },
    [idUser]
  );
  
  return contextUser.isLogged && contextUser.role.name === 'Administrator' ? (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>{idUser ? <Edit /> : <AddCircle />}</Avatar>
        <Typography component="h1" variant="h5">
          {idUser ? `User ${idUser}` : `New User`}
        </Typography>
        <form
          className={classes.form}
          onSubmit={async function(event) {
            event.preventDefault();

            const target = event.target;
            const data = new FormData(target);

            const handleThen = function(response) {
              enqueueSnackbar(
                `User ${response.data.id} was ${idUser ? 'updated' : 'saved'} successfully!`,
                { variant: 'success' }
              );

              history.push('/dashboard/u');
            };
            const handleCatch = function(response) {
              enqueueSnackbar('Fail! Please, check all the fields and try again.', {
                variant: 'error'
              });
            };

            if (idUser) {
              await axios
                .put(APIHelper.user(idUser), data)
                .then(handleThen)
                .catch(handleCatch);
            } else {
              await axios
                .post(APIHelper.users(), data)
                .then(handleThen)
                .catch(handleCatch);
            }
          }}
        >
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                defaultValue={idUser && 'Loading...'}
                value={user.name}
                onChange={handleChange}
                autoComplete="name"
                name="name"
                variant="outlined"
                required
                fullWidth
                id="name"
                label="Name"
                autoFocus
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                defaultValue={idUser && 'Loading...'}
                value={user.username}
                onChange={handleChange}
                variant="outlined"
                required
                fullWidth
                id="username"
                label="Username"
                name="username"
                autoComplete="username"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                defaultValue={idUser && 'Loading...'}
                value={user.password}
                onChange={handleChange}
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="password"
              />
            </Grid>
            <Grid item xs={12}>
              <InputLabel id="role_id" required>
                Role
              </InputLabel>
              <Select
                defaultValue={idUser && 'Loading...'}
                required
                fullWidth
                value={`${user.role_id}`}
                onChange={handleChange}
                labelId="role_id"
                id="role_id"
                name="role_id"
                input={<InputBase />}
              >
                <MenuItem>Select</MenuItem>
                {roles.map((role, index) => {
                  return (
                    <MenuItem key={`role_${index}`} value={role.id}>
                      {role.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            {idUser ? 'Edit' : 'Save'}
          </Button>
        </form>
      </div>
    </Container>
  ) : (
    <Redirect to="/" push />
  );
}

export default UserView;
