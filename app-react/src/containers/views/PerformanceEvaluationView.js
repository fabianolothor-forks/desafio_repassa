import React, { useContext, useEffect, useState } from 'react';

import { APIHelper } from '../../helpers';
import AddCircle from '@material-ui/icons/AddCircle';
import { AppContext } from '../../contexts';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Edit from '@material-ui/icons/Edit';
import Grid from '@material-ui/core/Grid';
import InputBase from '@material-ui/core/InputBase';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { Redirect } from 'react-router-dom';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

function PerformanceEvaluationView({ history, match }) {
  const [performanceEvaluation, setPerformanceEvaluation] = useState({});
  const [users, setUsers] = useState([]);

  const classes = useStyles();

  const { enqueueSnackbar } = useSnackbar();
  const { user: contextUser } = useContext(AppContext);
  const { idPerformanceEvaluation } = match.params || {};

  function handleChange(e) {
    setPerformanceEvaluation({
      ...performanceEvaluation,
      [e.target.name]: e.target.value
    });
  }

  useEffect(
    function() {
      axios.get(APIHelper.users()).then(function(response) {
        setUsers(response.data.filter(user => user.role.name !== 'Administrator'));
      });

      if (idPerformanceEvaluation) {
        axios
          .get(APIHelper.performanceEvaluation(idPerformanceEvaluation))
          .then(function(response) {
            setPerformanceEvaluation(response.data);
          });
      }
    },
    [idPerformanceEvaluation]
  );

  return contextUser.isLogged && contextUser.role.name === 'Administrator' ? (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          {idPerformanceEvaluation ? <Edit /> : <AddCircle />}
        </Avatar>
        <Typography component="h1" variant="h5">
          {idPerformanceEvaluation
            ? `Performance Evaluation ${idPerformanceEvaluation}`
            : `Performance Evaluation`}
        </Typography>
        <form
          className={classes.form}
          onSubmit={async function(event) {
            event.preventDefault();

            const target = event.target;
            const data = new FormData(target);

            const handleThen = function(response) {
              enqueueSnackbar(
                `Performance Evaluation ${response.data.id} was ${
                  idPerformanceEvaluation ? 'updated' : 'saved'
                } successfully!`,
                { variant: 'success' }
              );

              history.push('/dashboard/pe');
            };
            const handleCatch = function(response) {
              enqueueSnackbar('Fail! Please, check all the fields and try again.', {
                variant: 'error'
              });
            };

            if (data.get('employee_id') !== 'undefined') {
              if (idPerformanceEvaluation) {
                await axios
                  .put(APIHelper.performanceEvaluation(idPerformanceEvaluation), data)
                  .then(handleThen)
                  .catch(handleCatch);
              } else {
                await axios
                  .post(APIHelper.performanceEvaluations(), data)
                  .then(handleThen)
                  .catch(handleCatch);
              }
            } else {
              handleCatch();
            }
          }}
        >
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                defaultValue={idPerformanceEvaluation && 'Loading...'}
                value={performanceEvaluation.description}
                onChange={handleChange}
                autoComplete="description"
                name="description"
                variant="outlined"
                required
                fullWidth
                id="description"
                label="Performance Evaluation"
                autoFocus
                multiline
              />
            </Grid>
            <Grid item xs={12}>
              <InputLabel id="author_id" required>
                Author
              </InputLabel>
              <Select
                defaultValue={idPerformanceEvaluation && 'Loading...'}
                value={contextUser.id}
                fullWidth
                readOnly
                required
                disabled
                labelId="author_id"
                id="author_id"
                name="author_id"
                input={<InputBase />}
              >
                <MenuItem value={contextUser.id}>{contextUser.name}</MenuItem>
              </Select>
            </Grid>
            <Grid item xs={12}>
              <InputLabel id="employee_id" required>
                Employee
              </InputLabel>
              <Select
                required
                fullWidth
                defaultValue={idPerformanceEvaluation && 'Loading...'}
                value={`${performanceEvaluation.employee_id}`}
                onChange={handleChange}
                labelId="employee_id"
                id="employee_id"
                name="employee_id"
                input={<InputBase />}
              >
                <MenuItem>Select</MenuItem>
                {users.map((user, index) => {
                  return (
                    <MenuItem key={`employee_${index}`} value={user.id}>
                      {user.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            {idPerformanceEvaluation ? 'Edit' : 'Save'}
          </Button>
        </form>
      </div>
    </Container>
  ) : (
    <Redirect to="/" push />
  );
}

export default PerformanceEvaluationView;
