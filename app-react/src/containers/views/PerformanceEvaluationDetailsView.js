import React, { useContext, useEffect, useState } from 'react';

import { APIHelper } from '../../helpers';
import { AppContext } from '../../contexts';
import Container from '@material-ui/core/Container';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    position: 'relative',
    overflow: 'auto',
    textAlign: 'center'
  },
  listSection: {
    backgroundColor: 'inherit'
  },
  ul: {
    backgroundColor: 'inherit',
    padding: 0
  },
  li: {
    textAlign: 'center'
  }
}));

function PerformanceEvaluationDetailsView({ history, match }) {
  const [performanceEvaluation, setPerformanceEvaluation] = useState({});

  const classes = useStyles();

  const { user: contextUser } = useContext(AppContext);
  const { idPerformanceEvaluation } = match.params || {};

  useEffect(
    function() {
      if (idPerformanceEvaluation) {
        axios
          .get(APIHelper.performanceEvaluation(idPerformanceEvaluation))
          .then(function(response) {
            if (
              contextUser.role && contextUser.role.name !== 'Administrator' &&
              response.data.employee.id !== contextUser.id
            ) {
              history.push('/dashboard/pe');
            }

            setPerformanceEvaluation(response.data);
          });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [idPerformanceEvaluation]
  );

  return contextUser.isLogged ? (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <List className={classes.root} subheader={<li />}>
          <li className={classes.listSection}>
            <ul className={classes.ul}>
              <ListSubheader>Performance Evaluation</ListSubheader>
              <ListItem className={classes.li}>
                <ListItemText primary={`${performanceEvaluation.description}`} />
              </ListItem>
            </ul>
          </li>
        </List>
        {performanceEvaluation.author && (
          <List className={classes.root} subheader={<li />}>
            <li className={classes.listSection}>
              <ul className={classes.ul}>
                <ListSubheader>Author</ListSubheader>
                <ListItem className={classes.li}>
                  <ListItemText primary={`${performanceEvaluation.author.name}`} />
                </ListItem>
              </ul>
            </li>
          </List>
        )}
        {performanceEvaluation.employee && (
          <List className={classes.root} subheader={<li />}>
            <li className={classes.listSection}>
              <ul className={classes.ul}>
                <ListSubheader>Employee</ListSubheader>
                <ListItem className={classes.li}>
                  <ListItemText primary={`${performanceEvaluation.employee.name}`} />
                </ListItem>
              </ul>
            </li>
          </List>
        )}
        {performanceEvaluation.feedbacks &&
          performanceEvaluation.feedbacks.filter(feedback => feedback.description).length >
            0 && (
            <List className={classes.root} subheader={<li />}>
              <li className={classes.listSection}>
                <ul className={classes.ul}>
                  <ListSubheader>Feedbacks</ListSubheader>
                  {performanceEvaluation.feedbacks
                    .filter(feedback => feedback.description)
                    .map(feedback => (
                      <ListItem className={classes.li}>
                        <ListItemText
                          primary={`${feedback.description}`}
                          secondary={`${feedback.user.name}`}
                        />
                      </ListItem>
                    ))}
                </ul>
              </li>
            </List>
          )}
      </div>
    </Container>
  ) : (
    <Redirect to="/" push />
  );
}

export default PerformanceEvaluationDetailsView;
