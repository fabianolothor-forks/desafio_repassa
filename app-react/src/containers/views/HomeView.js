import React, { useContext } from 'react';

import { AppContext } from '../../contexts';
import { Redirect } from 'react-router-dom';

function HomeView() {
  const { user } = useContext(AppContext);

  return user.isLogged ? <Redirect to="/dashboard" push /> : <Redirect to="/sign-in" push />;
}

export default HomeView;
