import { AdminDashboard, UserDashboard } from '../../components';
import React, { useContext } from 'react';

import { AppContext } from '../../contexts';
import { Redirect } from 'react-router-dom';

function DashboardView() {
  const { user } = useContext(AppContext);

  return user.isLogged ? (
    user.role.name === 'Administrator' ? (
      <AdminDashboard />
    ) : (
      <UserDashboard />
    )
  ) : (
    <Redirect to="/sign-in" push />
  );
}

export default DashboardView;
