import React, { useContext, useEffect, useState } from 'react';

import { APIHelper } from '../../helpers';
import AddCircle from '@material-ui/icons/AddCircle';
import { AppContext } from '../../contexts';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Edit from '@material-ui/icons/Edit';
import Grid from '@material-ui/core/Grid';
import InputBase from '@material-ui/core/InputBase';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { Redirect } from 'react-router-dom';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

function PerformanceEvaluationFeedbackView({ history, match }) {
  const [performanceEvaluation, setPerformanceEvaluation] = useState({});
  const [user, setUser] = useState({});
  const [users, setUsers] = useState([]);

  const classes = useStyles();

  const { enqueueSnackbar } = useSnackbar();
  const { user: contextUser } = useContext(AppContext);
  const { idPerformanceEvaluation } = match.params || {};

  function handleChange(e) {
    setUser({
      ...user,
      [e.target.name]: e.target.value
    });
  }

  useEffect(
    function() {
      if (performanceEvaluation.id) {
        axios.get(APIHelper.users()).then(function(response) {
          setUsers(
            response.data.filter(
              user =>
                user.role.name !== 'Administrator' &&
                user.id !== performanceEvaluation.employee.id
            )
          );
        });
      }
    },
    [performanceEvaluation]
  );

  useEffect(
    function() {
      if (idPerformanceEvaluation) {
        axios
          .get(APIHelper.performanceEvaluation(idPerformanceEvaluation))
          .then(function(response) {
            setPerformanceEvaluation(response.data);
          });
      }
    },
    [idPerformanceEvaluation]
  );

  return contextUser.isLogged && contextUser.role.name === 'Administrator' ? (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <AddCircle />
        </Avatar>
        <Typography component="h1" variant="h5" align="center">
          Request Feedback to
          <br />
          Performance Evaluation {idPerformanceEvaluation}
        </Typography>
        <form
          className={classes.form}
          onSubmit={async function(event) {
            event.preventDefault();

            const target = event.target;
            const data = new FormData(target);

            const handleThen = function(response) {
              enqueueSnackbar(`Feedback request was sent to "${response.data.user.name}"!`, {
                variant: 'success'
              });

              history.push('/dashboard/pe');
            };
            const handleCatch = function(response) {
              enqueueSnackbar('Fail! Please, check all the fields and try again.', {
                variant: 'error'
              });
            };

            await axios
              .post(APIHelper.feedbacks(), data)
              .then(handleThen)
              .catch(handleCatch);
          }}
        >
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <InputLabel id="performance_evaluation_id" required>
                Performance Evaluation
              </InputLabel>
              <Select
                defaultValue={idPerformanceEvaluation && 'Loading...'}
                value={idPerformanceEvaluation}
                fullWidth
                readOnly
                required
                disabled
                labelId="performance_evaluation_id"
                id="performance_evaluation_id"
                name="performance_evaluation_id"
                input={<InputBase />}
              >
                <MenuItem value={idPerformanceEvaluation}>
                  Performance Evaluation {idPerformanceEvaluation}
                </MenuItem>
              </Select>
            </Grid>
            <Grid item xs={12}>
              <InputLabel id="user_id" required>
                Employee
              </InputLabel>
              <Select
                required
                fullWidth
                defaultValue={idPerformanceEvaluation && 'Loading...'}
                value={`${user.user_id}`}
                onChange={handleChange}
                labelId="user_id"
                id="user_id"
                name="user_id"
                input={<InputBase />}
              >
                <MenuItem>Select</MenuItem>
                {users.map((user, index) => {
                  return (
                    <MenuItem key={`user_${index}`} value={user.id}>
                      {user.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Request Feedback
          </Button>
        </form>
      </div>
    </Container>
  ) : (
    <Redirect to="/" push />
  );
}

export default PerformanceEvaluationFeedbackView;
