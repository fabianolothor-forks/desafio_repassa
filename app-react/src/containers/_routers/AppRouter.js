import {
  DashboardView,
  ErrorView,
  FeedbackView,
  HomeView,
  PerformanceEvaluationDetailsView,
  PerformanceEvaluationFeedbackView,
  PerformanceEvaluationView,
  SignInView,
  SignUpView,
  UserView
} from '..';
import { Route, Switch } from 'react-router-dom';

import React from 'react';

function AppRouter() {
  return (
    <Switch>
      <Route path="/" component={HomeView} exact />
      <Route path="/dashboard" component={DashboardView} exact />
      <Route path="/dashboard/feedback/:idFeedback(\d+)" component={FeedbackView} exact />
      <Route
        path="/dashboard/performance-evaluation"
        component={PerformanceEvaluationView}
        exact
      />
      <Route
        path="/dashboard/performance-evaluation/:idPerformanceEvaluation(\d+)"
        component={PerformanceEvaluationView}
        exact
      />
      <Route
        path="/dashboard/performance-evaluation/:idPerformanceEvaluation(\d+)/feedback"
        component={PerformanceEvaluationFeedbackView}
        exact
      />
      <Route
        path="/dashboard/performance-evaluation/:idPerformanceEvaluation(\d+)/view"
        component={PerformanceEvaluationDetailsView}
        exact
      />
      <Route path="/dashboard/user" component={UserView} exact />
      <Route path="/dashboard/user/:idUser(\d+)" component={UserView} exact />
      <Route path="/dashboard/:tab" component={DashboardView} exact />
      <Route path="/sign-in" component={SignInView} exact />
      <Route path="/sign-up" component={SignUpView} exact />

      <Route component={ErrorView} exact />
    </Switch>
  );
}

export default AppRouter;
