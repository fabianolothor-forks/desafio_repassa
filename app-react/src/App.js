import { AppCustomContextProvider } from './contexts';
import { AppRouter } from './containers';
import React from 'react';
import { Template } from './components';

function App() {
  return (
    <>
      <AppCustomContextProvider>
        <Template>
          <AppRouter />
        </Template>
      </AppCustomContextProvider>
    </>
  );
}

export default App;
