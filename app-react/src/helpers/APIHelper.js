const API_URL = 'http://localhost:3000/api/v1';

const APIHelper = {
  feedback: id => `${APIHelper.feedbacks()}/${id}`,
  feedbacks: idUser =>
    idUser ? `${APIHelper.url()}/feedbacks/user/${idUser}` : `${APIHelper.url()}/feedbacks`,
  performanceEvaluation: id => `${APIHelper.performanceEvaluations()}/${id}`,
  performanceEvaluations: idUser =>
    idUser
      ? `${APIHelper.url()}/performance_evaluations/user/${idUser}`
      : `${APIHelper.url()}/performance_evaluations`,
  role: id => `${APIHelper.roles()}/${id}`,
  roles: () => `${APIHelper.url()}/roles`,
  signIn: () => `${APIHelper.users()}/sign_in`,
  url: () => API_URL,
  user: id => `${APIHelper.users()}/${id}`,
  users: () => `${APIHelper.url()}/users`
};

export default APIHelper;
